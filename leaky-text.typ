#import "isotope.typ": *
#show: doc => isotope-report(
    title: "The Leaky Semicolon: Text Proofs",
    authors: (
        (
            name: "Jad Elkhaleq Ghalayini",
            affiliation: "University of Cambridge",
            email: "jeg74@cl.cam.ac.uk"
        ),
    ),
    doc
)

= Pomsets with Predicate Transformers

#definition(name: "Predicate Transformer")[
    A *predicate transformer* is a function $τ: Φ -> Φ$ s.t.
    \ (X1): τ(ψ_1 ∧ ψ_2) = τ(ψ_1) ∧ τ(ψ_2)
    \ (X2): τ(ψ_1 ∨ ψ_2) = τ(ψ_1) ∨ τ(ψ_2)
    \ (X3): If $φ ⊧ ψ$, then $τ(φ) ⊧ τ(ψ)$
]
Examples of predicate transformers include:
- The identity $τ(ψ) = ψ$
- Constant functions $τ(-) = ψ$
- Substitutions $τ(ψ) = ψ[ssub(M, r)]$ or $τ(ψ) = ψ[ssub(M, x)]$
- Implications on the right $τ(ψ) = φ => ψ$
As predicate transformers are closed under composition, they form a category. We say a predicate transformer $τ$ *entails* $τ'$, written $τ ⊧ τ'$, if $∀ψ, τ(ψ) ⊧ τ(ψ')$. This relation is transitive, reflexive, and antisymmetric up to equivalence, so is a partial order on predicate transformers up to equivalence (and, otherwise, a preorder).

#definition[
    A *family of predicate transformers* over a set $E ⊆ cal(E)$ is a map assigning every $D ⊆ cal(E)$ a predicate transformer $τ^D$ such that
    $
    ∀C ⊆ cal(E), C ∩ E ⊆ D => τ^C ⊧ τ^D
    $
    Equivalently, it is (up to equivalence) a map assigning every $D ⊆ E$ a predicate transformer $τ^D$ such that
    $
    ∀C ⊆ E, C ⊆ D => τ^C ⊧ τ^D 
    $
    extended to $D ⊆ cal(E)$ by taking $τ^D = τ^(D ∩ E)$; in this way, it can be viewed as a morphism from $cal(P)(E)$ to the set of predicate transformers in the category of partial orders.
]

#definition(name: "Pomset with Predicate Transformers")[
    A *pomset with predicate transformers* (*PwT*) is a tuple $(E, λ, κ, τ, ✓, <)$ such that
    \ (M1): $E ⊆ cal(E)$ is a set of *events*
    \ (M2): $λ: E → cal(A)$ labels each event with an *action*
    \ (M3): $κ: E -> Φ$ defines a *precondition* for each event
    \ (M4): $τ: cal(P)(cal(E)) -> Φ -> Φ$ is a family of predicate transformers over $E$
    \ (M5): $✓: Φ$ is a *termination condition*
    \ (M5a): $✓ ⊧ τ^E(⊤)$
    \ (M6): $< ⊆ E × E$ is a strict partial order capturing *causality*
    
    A PwT is *complete* if
    \ (C3): $∀e ∈ E, κ(e)$ is a tautology
    \ (C5): $✓$ is a tautology  

    We will extend $κ$ such that, for $e ∉ E$, $κ(e) = ⊥$. This recovers the original definition.
]

= PwT Semantics of Programs

We define:
$
sans("skip") &= {{E = ∅, τ^D = idm, ✓ = ⊤}} \
sans("assign")(r, M) &= {{E = ∅, τ^D(ψ) = ψ[ssub(M, r)], ✓ = ⊤}} \
sans("par")(cal(P)_1, cal(P)_2) &= {{
        E = E_1 ⊔ E_2,
    \ & #h(3em)
        λ = λ_1 ∪ λ_2,
    \ & #h(3em)
        κ(e) = κ_1(e) ∨ κ_2(e), 
    \ & #h(3em)
        τ^D = τ_2^D,
    \ & #h(3em)
        ✓ = ✓_1 ∧ ✓_2,
    \ & #h(3em)
        <
    } aq | aq P_1 ∈ cal(P)_1, P_2 ∈ cal(P)_2, (<) ⊇ (<_1) ∪ (<_2)
} \
sans("seq")(cal(P)_1, cal(P)_2) &= {{
        E = E_1 ∪ E_2,
    \ & #h(3em)
        λ = λ_1 ∪ λ_2,
    \ & #h(3em)
        κ(e) = lif e ∈ E_2 aq { κ_1(e) ∨ τ_1^(C_1(e, E_1, λ(e), <))(κ_2(e)) } lelse { κ_1(e) }
    \  & #h(6em) "where" C(e, E_1, a, <) = lif a "read" aq {{c ∈ cal(E) | c < e}} lelse {E_1}, 
    \ & #h(3em)
        τ^D = τ_1^D ∘ τ_2^D,
    \ & #h(3em)
        ✓ = ✓_1 ∧ τ_1^(E_1)(✓_2),
    \ & #h(3em)
        <
    } aq | aq P_1 ∈ cal(P_1), P_2 ∈ cal(P)_2, (<) ⊇ (<_1) ∪ (<_2)
} \
sans("if")(φ, cal(P)_1, cal(P)_2) &= {{
        E = E_1 ∪ E_2,
    \ & #h(3em)
        λ = λ_1 ∪ λ_2,
    \ & #h(3em)
        κ(e) = (ϕ ∧ κ_1(e)) ∨ (ϕ ∧ κ_2(e)),
    \ & #h(3em)
        τ^D(ψ) = (φ ∧ τ_1^D(ψ)) ∨ (¬φ ∧ τ_2^D(ψ)),
    \ & #h(3em)
        ✓ = (φ ∧ ✓_1) ∨ (¬φ ∧ ✓_2),
    \ & #h(3em)
        <
    } aq | aq P_1 ∈ cal(P_1), P_2 ∈ cal(P)_2, (<) ⊇ (<_1) ∪ (<_2)
}
$
$
sans("fence")(μ) &= {{
        E,
    \ & #h(3em)
        λ(e) = sans("F")^μ,
    \ & #h(3em)
        κ(e) = ⊤,
    \ & #h(3em)
        τ^D = idm,
    \ & #h(3em)
        ✓ = (|E| > 0)
    \ & #h(3em) } aq | aq E ⊆ cal(E), |E| ≤ 1
} \
sans("write")(x, M, μ) &= {{
        E,
    \ & #h(3em)
        λ(e) = sans("W")^μ x v,
    \ & #h(3em)
        κ(e) = (M = v),
    \ & #h(3em)
        τ^D(ψ) = ψ[ssub(M, x)][ssub(✓, sans("Q")_x)],
    \ & #h(3em)
        ✓ = (|E| > 0) ∧ (M = v)
    \ & #h(3em) } aq | aq E ⊆ cal(E), |E| ≤ 1, v ∈ cal(V)
} \
sans("read")(r, x, μ) &= {{
        E,
    \ & #h(3em)
        λ(e) = sans("R")^μ x v,
    \ & #h(3em)
        κ(e) = sans("Q")_x,
    \ & #h(3em)
        τ^D = lif e ∈ E ∩ D aq { (κ(e) => v = r) => ψ }
    \ & #h(6em) lelse lif e ∈ E backslash D aq { 
            (κ(e) => (v = r ∨ x = r)) => ψ } 
    \ & #h(6em) lelse { ψ },
    \ & #h(3em)
        ✓ = μ ⊑ sans("rlx") ∨ ((|E| > 0) ∧ sans("Q")_x)
    \ & #h(3em) } aq | aq E ⊆ cal(E), |E| ≤ 1, v ∈ cal(V)
} \
$

== Properties

#theorem(name: "Sequential composition is a monoid")[
    For all $cal(P)$,
    $sans("seq")(sans("skip"), cal(P)) = sans("seq")(cal(P), sans("skip")) = cal(P)$

    For all $cal(P)_1, cal(P)_2, cal(P)_3$, 
    $sans("seq")(cal(P)_1, sans("seq")(cal(P)_2, cal(P)_3))
    = sans("seq")(sans("seq")(cal(P)_1, cal(P)_2), cal(P)_3)$
]
#proof[
    Fix PwT $cal(P)$. Assume $cal(P)_l ∈ sans("seq")(sans("skip"), cal(P))$, $cal(P)_r ∈ sans("seq")(cal(P), sans("skip"))$. We have that
    $
    E &= E_l &= ∅ ∪ E &= E_r &= E ∪ ∅ & \
    λ &= λ_l &= ∅ ∪ λ_l &= λ_r &= λ_r ∪ ∅ \
    κ(e) &= κ_l(e) &= lif ⊥ { ... } lelse { κ(e) } &= κ_r(e) &= lif e ∈ E aq { ⊥ ∨ κ(e) } lelse { κ(e) } & \
    τ^D &= τ_l^D &= idm ∘ τ^D &= τ_r^D &= τ^D ∘ idm \
    ✓ &= ✓_l &= ⊤ ∧ idm(✓) &= ✓_r &= ✓ ∧ τ^E(⊤) & "by M5a" \
    (<) &= (<_l) &= (<_r)
    $
    It follows that
    $
    sans("seq")(sans("skip"), cal(P)) = sans("seq")(cal(P), sans("skip")) = cal(P)
    $
    as desired.
    
    Fix PwTs $cal(P)_1, cal(P)_2, cal(P)_3$.
    
    Assume $P ∈ sans("seq")(cal(P)_1, sans("seq")(cal(P)_2, cal(P)_3))$.
    Then there exist $P_1 ∈ cal(P)_1$, $P_(2, 3) ∈ sans("seq")(cal(P)_2, cal(P)_3)$, $P_2 ∈ cal(P)_2$, $P_3 ∈ cal(P)_3$, $P_(1, 2) ∈ sans("seq")(cal(P)_1, cal(P)_2)$ such that 
    $
    &#h(30em) & \
    E &= E_1 ∪ E_(2, 3)
        = E_1 ∪ (E_2 ∪ E_3) 
        = (E_1 ∪ E_2) ∪ E_3
        = E_(1, 2) ∪ E_3 
        \ 
    λ &= λ_1 ∪ λ_(2, 3) 
        = λ_1 ∪ (λ_2 ∪ λ_3)
        = (λ_1 ∪ λ_2) ∪ λ_3
        = λ_(1, 2) ∪ λ_3 
        \ 
    κ(e) &= lif e ∈ E_(2, 3) aq { κ_1(e) ∨ τ_1^(C(e, E_1, λ(e), <))(κ_(2, 3)(e)) } lelse { κ_1(e) }
        \ &= lif e ∈ E_(2, 3) { κ_1(e) ∨ τ_1^(C(e, E_1, λ(e), <))
        \ & #h(7em) (lif e ∈ E_3 { κ_2(e) ∨ τ_2^(C(e, E_2, λ_(2, 3)(e), <))(κ_3(e)) } lelse { κ_2(e) })} 
        \ & #h(1em) lelse { κ_1(e) }
        \ &= lif e ∈ E_3 {
            κ_1(e) ∨ τ_1^(C(e, E_1, λ(e), <))(κ_2(e) ∨ τ_2^(C(e, E_2, λ_(2, 3)(e), <))(κ_3(e)))
        } 
        \ & #h(1em) lelse lif e ∈ E_2 {
            κ_1(e) ∨ τ_1^(C(e, E_1, λ(e), <))(κ_2(e))
        }
        \ & #h(1em) lelse { κ_1(e) }
        \ &= lif e ∈ E_3 { κ_(1, 2)(e) ∨ (τ_1^(C(e, E_(1, 2), λ(e), <)) ∘ τ_2^(C(e, E_(1, 2), λ(e), <)))(κ_3(e)) } lelse { κ_(1, 2)(e) } 
        \ &= lif e ∈ E_3 { κ_(1, 2)(e) ∨ τ_(1, 2)^(C(e, E_(1, 2), λ(e), <))(κ_3(e)) } lelse { κ_(1, 2)(e) } 
        \
    τ^D &= τ_1^D ∘ τ_(2, 3)^D 
        = τ_1^D ∘ (τ_2^D ∘ τ_3^D)
        = (τ_1^D ∘ τ_2^D) ∘ τ_3^D 
        = τ_(1, 2)^D ∘ τ_3^D 
        \ 
    ✓ &= ✓_1 ∧ τ_1^(E_1)(✓_(2, 3)) 
        = ✓_1 ∧ τ_1^(E_1)(✓_2 ∧ τ_2^(E_2)(✓_3))
            & "by S5" 
        \ &= ✓_1 ∧ τ_1^(E_1)(✓_2) ∧ (τ_1^(E_1) ∘ τ_2^(E_2))(✓_3)
            & "by X1"
        \ &= ✓_1 ∧ τ_1^(E_1)(✓_2) ∧ (τ_1^(E_(1, 2)) ∘ τ_2^(E_(1, 2)))(✓_3) 
            & "by defn. 3.3"
        \ &= ✓_(1, 2) ∧ τ_(1, 2)^(E_(1, 2))(✓_3) & "by definition"
        \
    (<) &"respects" (<_(1, 2)) "by definition, and" (<_3) "since" (<_(2, 3)) "respects" (<_3)
    $
    where
    $
    E_(1, 2) &= E_1 ∪ E_2 \
    λ_(1, 2) &= λ_1 ∪ λ_2 \
    κ_(1, 2) &= lif e ∈ E_2 aq { κ_1(e) ∨ τ_1^(C(e, E_1, λ_1, <))(κ_2(e)) } lelse { κ_1(e) } \
    τ_(1, 2)^D &= τ_1^D ∘ τ_2^D \
    ✓_(1, 2) &= ✓_1 ∧ τ_1^(E_1)(✓_2) \
    (<_(1, 2)) &= (<)_(E_(1, 2) × E_(1, 2))
    $
    Hence, by definition, $P ∈ sans("seq")(sans("seq")(cal(P)_1, cal(P)_2), cal(P)_3)$

    Now, assume $P ∈ sans("seq")(sans("seq")(cal(P)_1, cal(P)_2), cal(P)_3)$.
    Then there exist $P_1 ∈ cal(P)_1$, $P_(2, 3) ∈ sans("seq")(cal(P)_2, cal(P)_3)$, $P_2 ∈ cal(P)_2$, $P_3 ∈ cal(P)_3$, $P_(1, 2) ∈ sans("seq")(cal(P)_1, cal(P)_2)$ such that 
        $
    &#h(30em) & \
    E &= E_(1, 2) ∪ E_3
        = (E_1 ∪ E_2) ∪ E_3
        = E_1 ∪ (E_2 ∪ E_3) 
        = E_1 ∪ E_(2, 3)
        \ 
    λ &= λ_(1, 2) ∪ λ_3
        = (λ_1 ∪ λ_2) ∪ λ_3
        = λ_1 ∪ (λ_2 ∪ λ_3) 
        = λ_1 ∪ λ_(2, 3) 
        \ 
    κ(e) &= lif e ∈ E_3 { κ_(1, 2)(e) ∨ τ_(1, 2)^(C(e, E_(1, 2), λ(e), <))(κ_3(e)) } lelse { κ_(1, 2)(e) } 
        \ &= lif e ∈ E_3 { κ_(1, 2)(e) ∨ (τ_1^(C(e, E_(1, 2), λ(e), <)) ∘ τ_2^(C(e, E_(1, 2), λ(e), <)))(κ_3(e)) } lelse { κ_(1, 2)(e) } 
        \ &= lif e ∈ E_3 {
            κ_1(e) ∨ τ_1^(C(e, E_1, λ(e), <))(κ_2(e) ∨ τ_2^(C(e, E_2, λ_(2, 3)(e), <))(κ_3(e)))
        } 
        \ & #h(1em) lelse lif e ∈ E_2 {
            κ_1(e) ∨ τ_1^(C(e, E_1, λ(e), <))(κ_2(e))
        }
        \ & #h(1em) lelse { κ_1(e) }
        \ &= lif e ∈ E_(2, 3) { κ_1(e) ∨ τ_1^(C(e, E_1, λ(e), <))
        \ & #h(7em) (lif e ∈ E_3 { κ_2(e) ∨ τ_2^(C(e, E_2, λ_(2, 3)(e), <))(κ_3(e)) } lelse { κ_2(e) })} 
        \ & #h(1em) lelse { κ_1(e) }
        \ &= lif e ∈ E_(2, 3) aq { κ_1(e) ∨ τ_1^(C(e, E_1, λ(e), <))(κ_(2, 3)(e)) } lelse { κ_1(e) }
        \
    τ^D &= τ_(1, 2)^D ∘ τ_3^D 
        = (τ_1^D ∘ τ_2^D) ∘ τ_3^D 
        = τ_1^D ∘ (τ_2^D ∘ τ_3^D)
        = τ_1^D ∘ τ_(2, 3)^D 
        \ 
    ✓ &= ✓_(1, 2) ∧ τ_(1, 2)^(E_(1, 2))(✓_3) 
        = ✓_1 ∧ τ_1^(E_1)(✓_2) ∧ (τ_1^(E_(1, 2)) ∘ τ_2^(E_(1, 2)))(✓_3) 
        \ &= ✓_1 ∧ τ_1^(E_1)(✓_2) ∧ (τ_1^(E_1) ∘ τ_2^(E_2))(✓_3)
        \ &= ✓_1 ∧ τ_1^(E_1)(✓_2 ∧ τ_2^(E_2)(✓_3))
        \ &= ✓_1 ∧ τ_1^(E_1)(✓_(2, 3)) 
        \ &
        \
    (<) &"respects" (<_(2, 3)) "by definition, and" (<_1) "since" (<_(1, 2)) "respects" (<_1)
    $
    where
    $
    E_(2, 3) &= E_2 ∪ E_3 \
    λ_(2, 3) &= λ_2 ∪ λ_3 \
    κ_(2, 3) &= lif e ∈ E_3 aq { κ_2(e) ∨ τ_2^(C(e, E_2, λ_2, <))(κ_3(e)) } lelse { κ_2(e) } \
    τ_(2, 3)^D &= τ_2^D ∘ τ_3^D \
    ✓_(2, 3) &= ✓_2 ∧ τ_1^(E_2)(✓_3) \
    (<_(2, 3)) &= (<)_(E_(2, 3) × E_(2, 3))
    $

    Hence, by definition, $P ∈ sans("seq")(cal(P)_1, sans("seq")(cal(P)_2, cal(P)_3))$. It follows that $
    sans("seq")(cal(P)_1, sans("seq")(cal(P)_2, cal(P)_3))
    = sans("seq")(sans("seq")(cal(P)_1, cal(P)_2), cal(P)_3)
    $ as desired
]

#definition(name: "Augment-closed PwTs")[
    A set of PwTs $cal(P)$ is *augment-closed* if, for all $P ∈ cal(P)$, if $P'$ is a PwT equal to $P$ except for $(<') ⊇ (<)$ (i.e. $P'$ *augments* $P$), then $P' ∈ cal(P)$
]

#theorem(name: "Augment closure")[
    If the inputs to a leaky semicolon operator are augment closed, then the set of outputs is augment closed. 
]
#proof[
    $sans("skip")$, $sans("assign")$, $sans("fence")$, $sans("write")$, and $sans("read")$ are trivially augment closed by the uniqueness of the partial order on the empty and unit sets.

    - $sans("par")$: TODO: this
    - $sans("seq")$: TODO: this
    - $sans("if")$: TODO: this
]

#definition(name: "Nameless PwTs")[
    Given a bijection $σ: E' -> E$ for $E, E' ⊆ cal(E)$, we define the *event renaming* of a PwT $P$, $P_σ$, to be given by:
    $
    E_σ &= E' \
    λ_σ &= λ ∘ σ \
    κ_σ &= κ ∘ σ \
    τ_σ^D &= τ^(σ(D)) \
    ✓_σ &= ✓ \
    (<_σ) &= (<) ∘ (σ × σ)
    $

    A set of PwTs $cal(P)$ is *nameless* if, for all $P ∈ cal(P)$, for all bijections $σ: E' -> E$, if $P_σ ∈ cal(P)$
]

#theorem(name: "Nameless closure")[
    If the inputs to a leaky semicolon operator are nameless, then the set of outputs is nameless. 
]
#proof[
    $sans("skip")$, $sans("assign")$, $sans("fence")$, $sans("write")$, and $sans("read")$ are trivially nameless closed.

    - $sans("par")$: TODO: this
    - $sans("seq")$: TODO: this
    - $sans("if")$: TODO: this
]

= While-loops and Nontermination

//TODO: this